advent_of_code::solution!(5);

use std::cmp::max;
use std::cmp::min;
use std::u64::MAX;

// NOTE: Using Chomp again just to save time
//       Next time I plan to use something more maintained
use chomp::ascii::*;
use chomp::prelude::*;
use chomp::*;

pub fn part_one(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    let almanac = parse1(input).unwrap();
    almanac
        .seeds
        .iter()
        .map(|v| almanac.stages.iter().fold(*v, stage_func1))
        .min()
}

pub fn part_two(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    let almanac = parse2(input).unwrap();
    let ret = almanac.stages.iter().fold(almanac.seeds, |seeds, stage| {
        // println!("NEXT STAGE ------------ {:?}", stage);
        stage_func2(&seeds, stage)
    });
    Some(get_min_value(&ret))
}

fn stage_func1(v: u64, stage: &Stage) -> u64 {
    stage
        .iter()
        .find_map(|m| {
            if m.src <= v && m.src + m.len > v {
                Some(m.dest + (v - m.src))
            } else {
                None
            }
        })
        .unwrap_or(v)
}

// NOTE: Clippy is great! Asked me to use a slice instead of Vec, and then to use to_vec instead of copied()
fn stage_func2(vs: &[Range], stage: &Stage) -> Vec<Range> {
    // NOTE: Had to create a copy to keep the borrow checker happy
    let vsc: Vec<Range> = vs.to_vec(); // iter().copied().collect();

    // NOTE: Mutation!
    let mut done: Vec<Range> = Vec::new();
    let rem = stage.iter().fold(vsc, |vsc, m| {
        vsc.iter()
            .flat_map(|v| match map_range(m, v) {
                (Some(r), rem) => {
                    done.push(r);
                    rem
                }
                (None, rem) => rem,
            })
            .collect()
    });
    done.extend(rem);
    done
}

fn range_from_to(start: u64, end: u64) -> Option<Range> {
    if start < end {
        Some(Range {
            start,
            len: end - start,
        })
    } else {
        None
    }
}

fn map_offset(offset: i64, range: Range) -> Range {
    Range {
        start: (range.start as i64 + offset) as u64,
        len: range.len,
    }
}

fn get_min_value(ranges: &[Range]) -> u64 {
    // println!("{:?}", ranges);
    ranges.iter().fold(
        MAX,
        |min, range| {
            if min <= range.start {
                min
            } else {
                range.start
            }
        },
    )
}

// Returns the mapped range (if any), and the ranges still unmapped
fn map_range(map: &Map, range: &Range) -> (Option<Range>, Vec<Range>) {
    let src_range = map_to_src_range(map);
    let rstart = range.start;
    let sstart = src_range.start;
    let rend = range.start + range.len;
    let send = src_range.start + src_range.len;
    let mappable_range = range_from_to(max(rstart, sstart), min(rend, send));
    // We check this separately because -
    //   This case also takes care of the case with no overlap
    //   The rest of the code assumes that the ranges are not totally distinct
    match mappable_range {
        Some(middle_range) => {
            let done = Some(map_offset(map.dest as i64 - map.src as i64, middle_range));
            let rem = [range_from_to(rstart, sstart), range_from_to(send, rend)]
                .into_iter()
                .flatten()
                .collect();
            (done, rem)
        }
        None => (None, vec![*range]),
    }
}

fn map_to_src_range(map: &Map) -> Range {
    Range {
        start: map.src,
        len: map.len,
    }
}

// NOTE: Had to make Range Copy to get it to compile
#[derive(Debug, Copy, Clone)]
struct Range {
    start: u64,
    len: u64,
}

#[derive(Debug)]
struct Map {
    src: u64,
    dest: u64,
    len: u64,
}

type Stage = Vec<Map>;

#[derive(Debug)]
struct Almanac<A> {
    seeds: Vec<A>,
    stages: Vec<Stage>,
}

fn parse1(input: &str) -> Option<Almanac<u64>> {
    parse_only(|i| parse_input(i, decimal), input.as_bytes()).ok()
}

fn parse2(input: &str) -> Option<Almanac<Range>> {
    parse_only(|i| parse_input(i, parse_range), input.as_bytes()).ok()
}

fn parse_input<A, I: U8Input>(
    i: I,
    parse_seed: impl Fn(I) -> SimpleResult<I, A>,
) -> SimpleResult<I, Almanac<A>> {
    chomp::parse! {i;
        let seeds = parse_seeds(parse_seed);
        let stages = sep_by1(parse_stage, |i| string(i, b"\n\n"));
        ret Almanac {seeds, stages}
    }
}

fn parse_range<I: U8Input>(i: I) -> SimpleResult<I, Range> {
    chomp::parse! {i;
        let start = decimal();
        skip_whitespace();
        let len = decimal();
        ret Range {start, len}
    }
}

fn parse_seeds<A, I: U8Input>(
    i: I,
    parse_seed: impl Fn(I) -> SimpleResult<I, A>,
) -> SimpleResult<I, Vec<A>> {
    parse! {i;
        string(b"seeds: ");
        let seeds = sep_by1(parse_seed, skip_whitespace);
        ret seeds
    }
}

fn parse_stage<I: U8Input>(i: I) -> SimpleResult<I, Stage> {
    parse! {i;
        skip_while(|c| c != b':');
        string(b":\n");
        let maps = sep_by1(parse_map, |i| string(i,b"\n"));
        ret maps
    }
}

fn parse_map<I: U8Input>(i: I) -> SimpleResult<I, Map> {
    parse! {i;
        let dest = decimal();
        skip_whitespace();
        let src = decimal();
        skip_whitespace();
        let len = decimal();
        ret Map {src, dest, len}
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, Some(35));
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
