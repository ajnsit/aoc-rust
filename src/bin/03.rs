use std::collections::HashMap;

advent_of_code::solution!(3);

pub fn part_one(input: &str) -> Option<usize> {
    let grid = parse(input);
    Some(
        grid.iter()
            .filter_map(|(Pos(x, y), feature)| match feature {
                Feature::Number(d, l) => {
                    grid.iter()
                        .find_map(|(Pos(cx, cy), candidate)| match candidate {
                            Feature::Symbol(_) => {
                                // NOTE: The *s are ugly, why do I need them for primitive ints?
                                if around(*x, *y, *l, *cx, *cy) {
                                    Some(*d)
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        })
                }
                _ => None,
            })
            .sum(),
    )
}

fn around(x: usize, y: usize, span: usize, cx: usize, cy: usize) -> bool {
    // Can't subtract from 0::usize, hence the 0 checks
    (x == 0 || cx >= x - 1) && cx < x + span + 1 && (y == 0 || cy >= y - 1) && cy <= y + 1
}

// NOTE: This was my original attempt. Don't consume. Need read only values, i.e. references
// fn around(Pos(x, y): &Pos, span: &usize, Pos(cx, cy): &Pos) -> bool {
//     // NOTE: Wow the &s are ugly
//     cx >= &(x - 1) && cx <= &(x + span + 1) && cy >= &(y - 1) && cy <= &(y + 1)
// }

pub fn part_two(input: &str) -> Option<usize> {
    // let grid = parse(&advent_of_code::template::read_file("examples", DAY));
    let grid = parse(input);
    Some(
        grid.iter()
            .filter_map(|(Pos(cx, cy), feature)| match feature {
                Feature::Symbol(r) => {
                    if r != "*" {
                        None
                    } else {
                        let parts = grid
                            .iter()
                            .filter_map(|(Pos(x, y), candidate)| match candidate {
                                Feature::Number(d, l) => {
                                    // NOTE: The *s are ugly, why do I need them for primitive ints?
                                    if around(*x, *y, *l, *cx, *cy) {
                                        Some(*d)
                                    } else {
                                        None
                                    }
                                }
                                _ => None,
                            })
                            .collect::<Vec<_>>();
                        if parts.len() == 2 {
                            Some(parts.iter().product::<usize>())
                        } else {
                            None
                        }
                    }
                }
                _ => None,
            })
            .sum(),
    )
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord)]
enum Feature {
    Symbol(String),
    Number(usize, usize),
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, PartialOrd, Ord)]
struct Pos(usize, usize);
type Grid = HashMap<Pos, Feature>;

// NOTE: Mutation 🦇🦇🦇
fn insert(grid: &mut Grid, current: Option<(Pos, String, bool)>) {
    if let Some((pos, str, _)) = current {
        grid.insert(
            pos,
            match str.parse::<usize>() {
                Ok(d) => Feature::Number(d, str.len()),
                Err(_) => Feature::Symbol(str.to_owned()),
            },
        );
    }
}

fn parse(input: &str) -> Grid {
    // NOTE: Mutation 🦇🦇🦇
    let mut grid: Grid = HashMap::new();
    for (row, l) in input.lines().enumerate() {
        let mut current: Option<(Pos, String, bool)> = None;
        for (col, c) in l.chars().enumerate() {
            if c == '.' {
                insert(&mut grid, current);
                current = None;
            } else {
                match &mut current {
                    Some((_, str, was_digit)) => {
                        if c.is_ascii_digit() != *was_digit {
                            insert(&mut grid, current);
                            let pos = Pos(col, row);
                            current = Some((pos, String::from(c), c.is_ascii_digit()));
                        } else {
                            str.push(c);
                        }
                    }
                    None => {
                        let pos = Pos(col, row);
                        current = Some((pos, String::from(c), c.is_ascii_digit()));
                    }
                }
            }
        }
        // Get the last one
        insert(&mut grid, current);
    }
    grid
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
