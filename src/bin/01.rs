advent_of_code::solution!(1);

pub fn part_one(input: &str) -> Option<u32> {
    input.lines().map(extract_num).sum()
}

pub fn part_two(input: &str) -> Option<u32> {
    input.lines().map(extract_spelled).sum()
}

fn extract_num(line: &str) -> Option<u32> {
    let digits = line
        .chars()
        .filter_map(|c| c.to_digit(10))
        .collect::<Vec<u32>>();
    digits
        .last()
        // NOTE: Chaining maybes is painful
        .and_then(|last| digits.first().map(|first| first * 10 + last))
}

#[derive(Debug)]
enum Digit {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}

// NOTE: No way to enumerate over all enum members
// NOTE: Need to manually specify size to arrays
const ALL_DIGITS: [Digit; 9] = [
    Digit::One,
    Digit::Two,
    Digit::Three,
    Digit::Four,
    Digit::Five,
    Digit::Six,
    Digit::Seven,
    Digit::Eight,
    Digit::Nine,
];

impl Digit {
    fn to_int(&self) -> u32 {
        match self {
            Digit::One => 1,
            Digit::Two => 2,
            Digit::Three => 3,
            Digit::Four => 4,
            Digit::Five => 5,
            Digit::Six => 6,
            Digit::Seven => 7,
            Digit::Eight => 8,
            Digit::Nine => 9,
        }
    }

    fn to_string(&self) -> &str {
        match self {
            Digit::One => "one",
            Digit::Two => "two",
            Digit::Three => "three",
            Digit::Four => "four",
            Digit::Five => "five",
            Digit::Six => "six",
            Digit::Seven => "seven",
            Digit::Eight => "eight",
            Digit::Nine => "nine",
        }
    }
}

fn extract_spelled(line: &str) -> Option<u32> {
    extract_first(line).and_then(|first| extract_last(line).map(|last| first * 10 + last))
}

fn extract_first(line: &str) -> Option<u32> {
    // NOTE: Had to lok up how to take the first character of a string
    let c = line.chars().next().unwrap();
    c.to_digit(10).or_else(|| {
        let n = ALL_DIGITS.iter().find(|d| line.starts_with(d.to_string()));
        match n {
            Some(d) => Some(d.to_int()),
            // NOTE: Had to lok up how to take the tail of a string
            None => extract_first(&line[1..]),
        }
    })
}

fn extract_last(line: &str) -> Option<u32> {
    let c = line.chars().next_back().unwrap();
    c.to_digit(10).or_else(|| {
        let n = ALL_DIGITS.iter().find(|d| line.ends_with(d.to_string()));
        match n {
            Some(d) => Some(d.to_int()),
            None => extract_last(&line[..line.len() - 1]),
        }
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
