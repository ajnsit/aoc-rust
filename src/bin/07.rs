advent_of_code::solution!(7);

use itertools::Itertools;
// NOTE: Using Nom instead of Chomp, as it's more standard
// Nom's documentation is not very helpful. A lot of this is trial and error.
// Especialy the difference between byte and string parsers, and streaming vs complete
use nom::{
    bytes::complete::take,
    character::complete::{digit1, line_ending, multispace0},
    combinator::map_res,
    multi::separated_list1,
    IResult,
};

pub fn part_one(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    // I am calling a set of cards a Hand. I am calling a set of cards + a bid, a Game, to avoid confusion
    let games = parse_input(input)?;
    Some(
        games
            .iter()
            // NOTE: itertools has sorted functions for iterators! Yay!
            .sorted_by_key(|game| (hand_type1(&game.hand), &game.hand))
            // NOTE: Iterator inspect is great for debugging!
            // .inspect(|game| println!("sorted: {:?}", game.hand))
            .enumerate()
            .map(|(i, game)| (i as u64 + 1) * game.bid)
            .sum(),
    )
}

pub fn part_two(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    let games = parse_input(input)?;
    // NOTE: Had to separate the mapping to get it to compile (compare with part 1)
    let jokered: Vec<_> = games
        .into_iter()
        .map(|game| Game {
            bid: game.bid,
            hand: game.hand.iter().map(j_to_joker).collect(),
        })
        .collect();
    // NOTE: Can't call sort_by_key directly, need to use sorted_by_key for some reason
    // jokered.sort_by_key(|game| (hand_type1(&game.hand), &game.hand));
    Some(
        jokered
            .iter()
            .sorted_by_key(|game| (hand_type2(&game.hand), &game.hand))
            .enumerate()
            .map(|(i, game)| (i as u64 + 1) * game.bid)
            .sum(),
    )
}

// NOTE: Had to create a new struct to hold this info
//       Just using (HandType, Hand) caused errors with borrowing in part_two sorted_by_key
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
struct HandInfo {
    typ: HandType,
    hand: Hand,
}

#[derive(Debug)]
struct Game {
    hand: Hand,
    bid: u64,
}

// NOTE: After so much Rust, I still don't know enough about &str vs String
type Hand = Vec<Card>;

// NOTE: Derive clone
#[derive(Debug, PartialOrd, PartialEq, Ord, Eq, Copy, Clone)]
enum HandType {
    // Smallest variant at the top
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

fn hand_type1(hand: &Hand) -> HandType {
    // NOTE: Couldn't figure out how to use group_by from itertools
    //       This was helpful - https://stackoverflow.com/questions/47885478/how-to-use-itertools-group-by-iterator-method-without-a-for-loop
    //       It's interesting that Rust types and error messages were very unhelpful here
    let grouping = hand
        .iter()
        .sorted()
        // NOTE: Error: Lifetime may not live long enough. Had to add a dereference, not sure why
        .group_by(|c| *c)
        .into_iter()
        .map(|(_, group)| group.count())
        .sorted()
        .collect::<Vec<_>>();
    hand_type_from_grouping(grouping)
}

fn hand_type2(hand: &Hand) -> HandType {
    let total = hand.len();
    let grouping = hand
        .iter()
        .filter(|c| **c != Card::Joker)
        .sorted()
        .group_by(|c| *c)
        .into_iter()
        .map(|(_, group)| group.count())
        .sorted()
        .collect::<Vec<_>>();
    let card_count: usize = grouping.iter().sum();
    let joker_count = total - card_count;
    let modified_grouping = add_jokers(grouping, joker_count);
    hand_type_from_grouping(modified_grouping)
}

fn add_jokers(mut grouping: Vec<usize>, jokers: usize) -> Vec<usize> {
    let oref = grouping.last_mut();
    match oref {
        Some(lastref) => {
            *lastref += jokers;
            grouping
        }
        None => vec![jokers],
    }
}

fn hand_type_from_grouping(grouping: Vec<usize>) -> HandType {
    // NOTE: How do you match over a vector?
    //       Slice patterns!
    match grouping[..] {
        [5] => HandType::FiveOfAKind,
        [1, 4] => HandType::FourOfAKind,
        [2, 3] => HandType::FullHouse,
        [1, 1, 3] => HandType::ThreeOfAKind,
        [1, 2, 2] => HandType::TwoPair,
        [1, 1, 1, 2] => HandType::OnePair,
        _ => HandType::HighCard,
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
enum Card {
    // Smallest variant at the top
    Joker,
    C(u64),
    T,
    J,
    Q,
    K,
    A,
}

fn j_to_joker(card: &Card) -> Card {
    match card {
        Card::J => Card::Joker,
        card => card.clone(),
    }
}

fn card(c: char) -> Card {
    match c {
        'A' => Card::A,
        'K' => Card::K,
        'Q' => Card::Q,
        'J' => Card::J,
        'T' => Card::T,
        _ => Card::C(c.to_digit(10).unwrap_or(2) as u64),
    }
}

fn parse_input(input: &str) -> Option<Vec<Game>> {
    let (_, games) = parse_games1(input).ok()?;
    Some(games)
}

fn parse_games1(input: &str) -> IResult<&str, Vec<Game>> {
    separated_list1(line_ending, parse_game(&card))(input)
}

// NOTE: Had to add a wildcard lifetime to get it to compile?!
//       Reading Rust's lifetime ellision rules, the rules change if you add
//       wildcard lifetimes. Super weird!
fn parse_game<F>(card_parser: &F) -> impl (Fn(&str) -> IResult<&str, Game>) + '_
where
    F: Fn(char) -> Card,
{
    move |input: &str| {
        // NOTE: Error: i32: nom::ToUsize is not satisfied
        //       Had to specify the type of 5.
        //       On the plus side, first time I'm using the integer literal with type syntax!
        let (input, hand) = take(5u64)(input)?;
        let (input, _) = multispace0(input)?;
        let (input, bid) = parse_u64(input)?;
        // NOTE: 1. I didn't realise I needed to wrap the return value in Ok()
        //       2. Then didn't realise I needed to return input as well (as it's the _remaining_ input)
        Ok((
            input,
            Game {
                hand: hand.chars().map(card_parser).collect(),
                bid,
            },
        ))
    }
}

// NOTE: Nom doesn't have an in-built number parser?! Took this from the documentation
fn parse_u64(input: &str) -> IResult<&str, u64> {
    map_res(digit1, str::parse)(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
