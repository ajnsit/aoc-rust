advent_of_code::solution!(4);

// NOTE: Just for fun, decided to use a monadic parser combinator library for this day
//       Picked the first web search result that looked reasonable

// NOTE: No idea what the 2 lines below do
// #[macro_use]
// extern crate chomp;

use std::collections::HashMap;
use std::collections::HashSet;

// NOTE: Ideally I would like more granular imports
//       But the IDE is not being helpful in automatically importing things from chomp
//       Is this due to heavy macro usage?
use chomp::ascii::*;
use chomp::prelude::*;
use chomp::*;

pub fn part_one(input: &str) -> Option<u32> {
    // let cards = parse(&advent_of_code::template::read_file("examples", DAY));
    let cards = parse(input);
    cards.map(|cards| cards.iter().map(|c| points(score_card(c))).sum())
}

// NOTE: Really enjoyed solving part 2.
pub fn part_two(input: &str) -> Option<u32> {
    // let cards = parse(&advent_of_code::template::read_file("examples", DAY));
    let cards = parse(input);
    // NOTE: Mutation! I've found fold/reduce to be pretty much useless in Rust
    //       Since the accumulator usually requires mutable modifications
    let mut counts: HashMap<u32, u32> = HashMap::new();
    let count: u32 = cards
        // NOTE: I need to understand why as_ref is required so frequently
        // .as_ref()
        .unwrap()
        .iter()
        .map(|card| {
            let index: u32 = card.idx;
            let score = score_card(card);
            let additional_count = *counts.get(&index).unwrap_or(&0);
            // EDGE CASE: Don't forget the original 1 card!
            let actual_count = additional_count + 1;
            let start: u32 = index + 1;
            let end: u32 = start + score;
            (start..end).for_each(|j| {
                // NOTE: Spent a while trying to find a modify method on hashmap
                //       Turns out, you can just get a mutable reference to a value
                // NOTE: I like the entry interface
                counts
                    .entry(j)
                    .and_modify(|s| *s += actual_count)
                    .or_insert(actual_count);
            });
            actual_count
        })
        .sum();
    Some(count)
}

#[derive(Debug)]
struct Card {
    idx: u32,
    winning: Vec<u32>,
    numbers: Vec<u32>,
}

fn score_card(card: &Card) -> u32 {
    let winning = card.winning.iter().copied().collect::<HashSet<u32>>();
    let numbers = card.numbers.iter().copied().collect::<HashSet<u32>>();
    // NOTE: usize vs u32
    numbers.intersection(&winning).count() as _
}

fn points(score: u32) -> u32 {
    if score > 0 {
        2u32.pow(score - 1)
    } else {
        0
    }
}

fn parse(input: &str) -> Option<Vec<Card>> {
    parse_only(parse_input, input.as_bytes()).ok()
}

// NOTE: Macros look weird, and the error messages are terrible
fn parse_input<I: U8Input>(i: I) -> SimpleResult<I, Vec<Card>> {
    sep_by1(i, parse_card, |i| satisfy(i, is_end_of_line))
}

fn parse_card<I: U8Input>(i: I) -> SimpleResult<I, Card> {
    // NOTE: Macros look weird, and the error messages are terrible
    //       But still way better than a series of bind/then/map
    //       For example, I ran into these:
    //         1. When there is a return value, use bind instead of then
    //         2. At the end I must use map instead of bind/then
    chomp::parse! {i;

    // NOTE: Need to use byte array instead of string because the docs for chomp don't have enough
    // examples for directly using string
    // For example, I need an array to use string, but I could find no way to convert an &str to &[char]
    // (I did find a lot of warnings about why I shouldn't do this)
    // My best attempt is as follows, which still fails -
    //   string("Card ".chars().collect::<Vec<char>>().try_into().unwrap());
    string(b"Card");
    skip_spaces();

    let idx:u32 = decimal();
    skip_spaces();
    string(b":");
    skip_spaces();

    // NOTE: Arguments to combinators must be functions that take input as an arg
    //       Note the confusion between `skip_spaces` and `skip_spaces()`
    let winning = sep_by1(decimal, skip_spaces);
    skip_spaces();
    string(b"|");
    skip_spaces();
    let numbers = sep_by1(decimal, skip_spaces);

    ret Card{idx, winning, numbers}
    }
}

// One or more spaces
fn skip_spaces<I: U8Input>(i: I) -> SimpleResult<I, ()> {
    skip_while(i, is_whitespace)
}

/*
*/

// fn parse_digit<I: U8Input>(i: I) -> SimpleResult<I, u8> {
//     // NOTE: Why is there no c.to_ascii_digit?
//     satisfy(i, |c| c.is_ascii_digit()).map(|c| c - b'0')
// }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, Some(13));
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
