advent_of_code::solution!(6);

// NOTE: Using Chomp again just to save time
//       Next time I plan to use something more maintained
use chomp::ascii::*;
use chomp::prelude::*;
use chomp::*;

pub fn part_one(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    let oraces = parse1(input);
    oraces.map(|races| races.into_iter().map(ways_to_win).product())
}

pub fn part_two(input: &str) -> Option<u64> {
    // let input = &advent_of_code::template::read_file("examples", DAY);
    let orace = parse2(input);
    orace.map(ways_to_win)
}

#[derive(Debug, Copy, Clone)]
struct Race {
    time: u64,
    win_distance: u64,
}

fn distance(button_time: u64, total_time: u64) -> u64 {
    if button_time < total_time {
        (total_time - button_time) * button_time
    } else {
        0
    }
}

fn win(race: Race, button_time: u64) -> bool {
    distance(button_time, race.time) > race.win_distance
}

fn ways_to_win(race: Race) -> u64 {
    (1..race.time).fold(
        0,
        |n, button_time| if win(race, button_time) { n + 1 } else { n },
    )
}

fn parse1(input: &str) -> Option<Vec<Race>> {
    parse_only(parse_input, input.as_bytes()).ok()
}

fn parse2(input: &str) -> Option<Race> {
    parse1(input).and_then(|races| {
        let (times, distances): (Vec<_>, Vec<_>) = races
            .into_iter()
            .map(|race: Race| (race.time.to_string(), race.win_distance.to_string()))
            .unzip();
        times.join("").parse::<u64>().ok().and_then(|time| {
            distances.join("").parse::<u64>().ok().map(|distance| Race {
                time,
                win_distance: distance,
            })
        })
    })
}

fn parse_input<I: U8Input>(i: I) -> SimpleResult<I, Vec<Race>> {
    chomp::parse! {i;
        string(b"Time:");
        skip_whitespace();
        let times: Vec<u64> = sep_by1(decimal, skip_whitespace);
        skip_whitespace();
        string(b"Distance:");
        skip_whitespace();
        let distances: Vec<u64> = sep_by1(decimal, skip_whitespace);
        // NOTE: Why no zip_with?
        ret times.into_iter().zip(distances.into_iter()).map(|(time,win_distance)| Race {time, win_distance}).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
