advent_of_code::solution!(2);

use std::cmp::max;

use regex::Regex;

// Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green

#[derive(Debug)]
struct Game {
    id: u32,
    turns: Vec<Turn>,
}

#[derive(Debug)]
struct Turn {
    red: u32,
    green: u32,
    blue: u32,
}

static MAX_COUNTS: Turn = Turn {
    red: 12,
    green: 13,
    blue: 14,
};

fn valid(turn: &Turn) -> bool {
    turn.red <= MAX_COUNTS.red && turn.blue <= MAX_COUNTS.blue && turn.green <= MAX_COUNTS.green
}

fn power(turn: &Turn) -> u32 {
    turn.red * turn.green * turn.blue
}

fn minim(game: &Game) -> Turn {
    game.turns.iter().fold(
        Turn {
            red: 0,
            green: 0,
            blue: 0,
        },
        // NOTE: Probably should have used mutation..
        //       Is the Rust compiler smart enough to know to reuse the accumulator turn?
        |mins, turn| Turn {
            red: max(mins.red, turn.red),
            blue: max(mins.blue, turn.blue),
            green: max(mins.green, turn.green),
        },
    )
}

pub fn part_one(input: &str) -> Option<u32> {
    let sum: u32 = parse(input)
        .iter()
        .filter(|game| game.turns.iter().all(valid))
        .map(|game| game.id)
        .sum();
    Some(sum)
}

pub fn part_two(input: &str) -> Option<u32> {
    let sum: u32 = parse(input).iter().map(|game| power(&minim(game))).sum();
    Some(sum)
}

fn parse(input: &str) -> Vec<Game> {
    let game_regex: Regex = Regex::new(r"Game (\d+): (.*)").unwrap();
    let color_count_regex: Regex = Regex::new(r"(\d+) (red|blue|green)").unwrap();

    input
        .lines()
        .map(|s| {
            let captures = game_regex.captures(s).unwrap();
            let id = captures[1].parse::<u32>().unwrap();
            let turns = captures[2]
                // NOTE: Clippy is great! It warned me about using a string instead of a char in the pattern
                .split(';')
                .map(|turn| {
                    // NOTE: I need to update this accumulator destructively
                    //       Rust does not seem to make it easy to update it from a fold
                    //       And if I'm doing it destructively, then might as well use foreach
                    // NOTE: And immediately clarity was lost. the variable below was named `turn`
                    //       earlier, but that shadowed the turn variable above. This caused compile
                    //       errors. With a fold, this variable would be anonymous.
                    let mut counts = Turn {
                        red: 0,
                        green: 0,
                        blue: 0,
                    };
                    turn.trim().split(',').for_each(|str| {
                        let color_captures = color_count_regex.captures(str.trim()).unwrap();
                        let count = color_captures[1].parse::<u32>().unwrap();
                        // NOTE: Need as_ref here to be able to pattern match on strings
                        match color_captures[2].as_ref() {
                            "red" => counts.red = count,
                            "green" => counts.green = count,
                            "blue" => counts.blue = count,
                            // NOTE: Took a second to realise that `pure unit` was `{}`
                            _ => {}
                        }
                    });
                    counts
                })
                .collect();
            Game { id, turns }
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let result = part_one(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }

    #[test]
    fn test_part_two() {
        let result = part_two(&advent_of_code::template::read_file("examples", DAY));
        assert_eq!(result, None);
    }
}
